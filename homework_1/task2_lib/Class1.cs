﻿using System;

namespace task2_lib
{
    public delegate int[] Del1(int x);

    public delegate void Del2(int[] x);

    public class Delegates
    {
        //еще можно просто остатки от деления на 10 добавлять в массив
        /// <summary>
        /// метод, преобразующий число в массив его цифр
        /// </summary>
        /// <param name="x"> Входное число. </param>
        /// <returns> Возвращает массив цифр положительного числа. </returns>
        public static int[] GetDigits(int a) =>
            Array.ConvertAll(a.ToString().ToCharArray(), digit => digit - '0');

        /// <summary>
        /// метод, выводящий на экран массив интов
        /// </summary>
        public static void PrintArr(int[] arr) =>
            Array.ForEach(arr, numb => Console.Write(numb + "\t"));
    }
}