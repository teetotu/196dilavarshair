﻿using System;

namespace task5
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            //тк не сказано, что нужен делегат-тип, я воспользовался "встроенными"
            //делегат для подсчета цкил. суммы, заданной в условии
            Func<int, double, double> multiply = (i, x) =>
            {
                double res = 1;
                for (int j = 1; j <= 5; j++)
                    res *= i * x / j;
                return res;
            };

            //делегат для подсчета цикл. произведения, заданной в условии
            Func<double, double> add = (x) =>
            {
                double res = 0;
                for (int i = 1; i <= 5; i++)
                    res += multiply(i, x);
                return res;
            };

            do
            {
                double num = 0;
                while (!double.TryParse(Console.ReadLine(), out num))
                    Console.WriteLine("введите число ");

                Console.WriteLine($"Вычисленная сумма : {add(num)}");

                Console.WriteLine("для завершения выполнения программы нажмите ESC");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}