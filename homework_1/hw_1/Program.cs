﻿using System;

namespace task1
{
    class Program
    {
        public delegate int Cast(double x);

        static void Main()
        {
            do
            {
                //делегат, привязанный к анон методу, 
                //который возвращает ближайшее к введенному параметру четное число
                Cast cast1 = delegate(double x) { return (int) x % 2 == 0 ? (int) x : (int) x + 1; };

                //делегат, привязанный к анон методу, который возвращает порядок числа
                Cast cast2 = delegate(double x) { return (int) Math.Log10(x) + 1; };

                double[] testNumbers = {123.5, 12.35};

                Console.WriteLine("порядок числа {0}: {1}\r\nближайшее четное целое число к {0}: {2}\r\n",
                    testNumbers[1], cast1(testNumbers[1]), cast2(testNumbers[1]));
                for (int i = 0; i < 2; i++)
                    Console.WriteLine("порядок числа {0}: {1}\r\nближайшее четное целое число к {0}: {2}\r\n",
                        testNumbers[i], cast1(testNumbers[i]), cast2(testNumbers[i]));

                cast1 += cast2;
                Console.WriteLine("порядок числа {0}: {1}\r\n", testNumbers[1], cast1(testNumbers[1]));

                //анонимные методы заменены на лямбдя выражения
                Cast combCast = x => (int) x % 2 == 0 ? (int) x : (int) x + 1;
                combCast += x => (int) Math.Log10(x) + 1;
                Console.WriteLine("для заверешния выполнения программы нажмите ESC");
                Console.WriteLine("для повтора нажмите ENTER");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}