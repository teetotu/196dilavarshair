﻿using System;

namespace task3_lib
{
    public static class StaticTempConverters
    {
        // Из Цельсия в Кельвина и обратно.
        public static double Celsius_To_Kelvin(double deg) => Math.Round(deg + 273.15);
        public static double Kelvin_To_Celsius(double deg) => Math.Round(deg - 273.15);

        // Из Цельсия в Реомюры и обратно.
        public static double Celsius_To_Reaumur(double deg) => Math.Round(deg * 1.25);
        public static double Reaumur_To_Celsius(double deg) => Math.Round(deg / 1.25);

        // Изшкалы Цельсия в Ранкины и обратно.
        public static double Celsius_To_Rankine(double deg) => Math.Round((deg + 273.15) * 9 / 5);
        public static double Rankine_To_Celsius(double deg) => Math.Round((deg - 491.67) * 5 / 9);
    }
}