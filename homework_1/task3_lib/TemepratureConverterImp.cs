﻿using System;

namespace task3_lib
{
    public class TemperatureConverterImp
    {
        /// <summary>
        /// метод, который переводит градусы цельсия в фаренгейты 
        /// </summary>
        /// <param name="deg">градусы цельсия</param>
        /// <returns>переведенное в градусы фаренгейта значение температуры</returns>
        public double Celsius_To_Farenheit(double deg) => Math.Round(9 * deg / 5 + 32);

        /// <summary>
        /// метод, который переводит градусы фаренгейта в градусы цельсия
        /// </summary>
        /// <param name="deg">градусы фаренгейта</param>
        /// <returns>переведенное в градусы цельсия значение температуры</returns>
        public double Farenheit_To_Celsius(double deg) => Math.Round(5 * (deg - 32) / 9);
    }
}