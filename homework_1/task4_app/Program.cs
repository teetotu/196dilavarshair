﻿using System;
using task4_lib;

namespace task4_app
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            // конкретный робот
            Robot rob = new Robot();

            Steps[] trace =
            {
                new Steps(rob.Backward),
                new Steps(rob.Backward),
                new Steps(rob.Left)
            };

            // сообщить координаты
            Console.WriteLine("Start:" + rob.Position());
            for (int i = 0; i < trace.Length; i++)
            {
                Console.WriteLine("Method={0}, Target={1}",
                    trace[i].Method, trace[i].Target);
                trace[i]();
            }

            Console.WriteLine(rob.Position()); // сообщить координаты
            
            Steps delR = new Steps(rob.Right); // направо
            Steps delL = new Steps(rob.Left); // налево
            Steps delF = new Steps(rob.Forward); // вперед
            Steps delB = new Steps(rob.Backward); // назад
            // шаги по диагоналям (многоадресные делегаты):
            Steps delRF = delR + delF;
            Steps delRB = delR + delB;
            Steps delLF = delL + delF;
            Steps delLB = delL + delB;
            delLB();
            Console.WriteLine(rob.Position()); // сообщить координаты
            delRB();
            Console.WriteLine(rob.Position()); // сообщить координаты
            Console.WriteLine("Для завершения нажмите любую клавишу.");
            Console.ReadKey();
        }
    }
}