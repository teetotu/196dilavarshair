﻿using System;
using task2_lib;

namespace task2
{
    class Program
    {
        //тестовое 5-значное число
        static int num = 22910;
        //массив из 10 2-значных чисел
        static int[] arrOfNums = { 15, 15, 65, 32, 61, 21, 91, 22, 22, 22 };

        public static Del1 del1 = Delegates.GetDigits;
        public static Del2 del2 = Delegates.PrintArr;

        static void Main()
        {
            do
            {
                Console.WriteLine("само число {0}", num);
                Console.Write("массив его цифр  ");
                del2(del1(num));
                Console.WriteLine();
                foreach (int num in arrOfNums)
                {
                    Console.WriteLine("само число {0}", num);
                    Console.Write("массив его цифр  ");
                    del2(del1(num));
                    Console.WriteLine();
                }

                // информация о делегатах.
                Console.WriteLine($"{del1.Method}  {del1.Target}");
                Console.WriteLine($"{del2.Method}  {del2.Target}");

                Console.WriteLine("для заверешния выполнения программы нажмите ESC");
                Console.WriteLine("для повтора нажмите ENTER");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}