﻿using System;
using System.Text.RegularExpressions;
using task3_lib;

namespace task3_app
{
    public delegate double DelegateConvertTemperature(double temp);

    class Program
    {
        static void Main(string[] args)
        {
            do
            {
                double temp = 0;
                Console.WriteLine("Enter the value in degrees Celsius");
                while (!double.TryParse(Console.ReadLine(), out temp))
                    Console.WriteLine("Enter a parsable temperature value");

                TemperatureConverterImp tempConv = new TemperatureConverterImp();
                /*
               delegateConvertTemperature celsiusToFarenheit = tempConv.CelToFar;
               delegateConvertTemperature farenheitToCelsius = tempConv.FarToCel;

               // тестирование делегатов
               Console.WriteLine(celsiusToFarenheit(25));
               Console.WriteLine(farenheitToCelsius(celsiusToFarenheit(25)));
               */

                DelegateConvertTemperature[] arrConv =
                {
                    tempConv.Celsius_To_Farenheit, tempConv.Farenheit_To_Celsius,
                    StaticTempConverters.Celsius_To_Kelvin, StaticTempConverters.Kelvin_To_Celsius,
                    StaticTempConverters.Celsius_To_Rankine, StaticTempConverters.Rankine_To_Celsius,
                    StaticTempConverters.Celsius_To_Reaumur, StaticTempConverters.Reaumur_To_Celsius
                };
                
                Array.ForEach(arrConv, del =>
                    Console.WriteLine($"{temp} " + $"({Regex.Replace(del.Method.ToString(), "Double", "")}" +
                                      $") {del(temp)} "));

                Console.WriteLine("для заверешния выполнения программы нажмите ESC");
                Console.WriteLine("для повтора нажмите ENTER");
            } while (Console.ReadKey(true).Key != ConsoleKey.Escape);
        }
    }
}